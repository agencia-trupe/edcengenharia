@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('categoria_id', 'Categoria') !!}
    {!! Form::select('categoria_id', $categorias , old('categoria_id'), ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa do Serviço') !!}
    @if($submitText == 'Alterar')
    @if($servico->capa)
    <img src="{{ url('assets/img/servicos/'.$servico->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_pt', 'Texto Serviço (PT)') !!}
        {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_en', 'Texto Serviço (EN)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_es', 'Texto Serviço (ES)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_pt', 'Imagem sobre Serviço (PT)') !!}
        @if($submitText == 'Alterar')
        @if($servico->imagem_pt)
        <img src="{{ url('assets/img/servicos/'.$servico->imagem_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('imagem_pt', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_en', 'Imagem sobre Serviço (EN)') !!}
        @if($submitText == 'Alterar')
        @if($servico->imagem_en)
        <img src="{{ url('assets/img/servicos/'.$servico->imagem_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('imagem_en', ['class' => 'form-control']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('imagem_es', 'Imagem sobre Serviço (ES)') !!}
        @if($submitText == 'Alterar')
        @if($servico->imagem_es)
        <img src="{{ url('assets/img/servicos/'.$servico->imagem_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
        @endif
        {!! Form::file('imagem_es', ['class' => 'form-control']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('servicos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>