@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">CATEGORIAS</h2>
</legend>


@if(!count($categorias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="categorias">
        <thead>
            <tr>
                <th scope="col">Titulo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($categorias as $categoria)
            <tr id="{{ $categoria->id }}">
                <td>{{ $categoria->titulo_pt }}</td>
                <td class="crud-actions">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('categorias.edit', $categoria->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection