@include('painel.layout.flash')

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_pt', 'Texto Categoria (PT)') !!}
        {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_en', 'Texto Categoria (EN)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_es', 'Texto Categoria (ES)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('vantagem1_icone', 'Vantagem 1 - Ícone') !!}
    @if($categoria->vantagem1_icone)
    <img src="{{ url('assets/img/categorias/'.$categoria->vantagem1_icone) }}" style="display:block; margin-bottom: 10px; width:auto; max-width: 100%;">
    @endif
    {!! Form::file('vantagem1_icone', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem1_titulo_pt', 'Vantagem 1 - Título (PT)') !!}
        {!! Form::text('vantagem1_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem1_titulo_en', 'Vantagem 1 - Título (EN)') !!}
        {!! Form::text('vantagem1_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem1_titulo_es', 'Vantagem 1 - Título (ES)') !!}
        {!! Form::text('vantagem1_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem1_texto_pt', 'Vantagem 1 - Texto (PT)') !!}
        {!! Form::textarea('vantagem1_texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem1_texto_en', 'Vantagem 1 - Texto (EN)') !!}
        {!! Form::textarea('vantagem1_texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem1_texto_es', 'Vantagem 1 - Texto (ES)') !!}
        {!! Form::textarea('vantagem1_texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('vantagem2_icone', 'Vantagem 2 - Ícone') !!}
    @if($categoria->vantagem2_icone)
    <img src="{{ url('assets/img/categorias/'.$categoria->vantagem2_icone) }}" style="display:block; margin-bottom: 10px; width:auto; max-width: 100%;">
    @endif
    {!! Form::file('vantagem2_icone', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem2_titulo_pt', 'Vantagem 2 - Título (PT)') !!}
        {!! Form::text('vantagem2_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem2_titulo_en', 'Vantagem 2 - Título (EN)') !!}
        {!! Form::text('vantagem2_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem2_titulo_es', 'Vantagem 2 - Título (ES)') !!}
        {!! Form::text('vantagem2_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem2_texto_pt', 'Vantagem 2 - Texto (PT)') !!}
        {!! Form::textarea('vantagem2_texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem2_texto_en', 'Vantagem 2 - Texto (EN)') !!}
        {!! Form::textarea('vantagem2_texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem2_texto_es', 'Vantagem 2 - Texto (ES)') !!}
        {!! Form::textarea('vantagem2_texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('vantagem3_icone', 'Vantagem 3 - Ícone') !!}
    @if($categoria->vantagem3_icone)
    <img src="{{ url('assets/img/categorias/'.$categoria->vantagem3_icone) }}" style="display:block; margin-bottom: 10px; width:auto; max-width: 100%;">
    @endif
    {!! Form::file('vantagem3_icone', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem3_titulo_pt', 'Vantagem 3 - Título (PT)') !!}
        {!! Form::text('vantagem3_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem3_titulo_en', 'Vantagem 3 - Título (EN)') !!}
        {!! Form::text('vantagem3_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem3_titulo_es', 'Vantagem 3 - Título (ES)') !!}
        {!! Form::text('vantagem3_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem3_texto_pt', 'Vantagem 3 - Texto (PT)') !!}
        {!! Form::textarea('vantagem3_texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem3_texto_en', 'Vantagem 3 - Texto (EN)') !!}
        {!! Form::textarea('vantagem3_texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('vantagem3_texto_es', 'Vantagem 3 - Texto (ES)') !!}
        {!! Form::textarea('vantagem3_texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('categorias.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>