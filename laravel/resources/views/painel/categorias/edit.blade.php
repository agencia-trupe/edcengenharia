@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CATEGORIAS |</small> Editar Categoria</h2>
</legend>

{!! Form::model($categoria, [
'route' => ['categorias.update', $categoria->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.categorias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection