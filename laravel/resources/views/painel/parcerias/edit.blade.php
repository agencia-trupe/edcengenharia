@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PARCERIAS |</small> Editar Parceria</h2>
</legend>

{!! Form::model($parceria, [
'route' => ['parcerias.update', $parceria->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.parcerias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection