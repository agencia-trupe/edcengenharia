@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PARCERIAS |</small> Adicionar Parceria</h2>
</legend>

{!! Form::open(['route' => 'parcerias.store', 'files' => true]) !!}

@include('painel.parcerias.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection