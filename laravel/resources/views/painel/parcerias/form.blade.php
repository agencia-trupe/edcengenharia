@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('imagem', 'Imagem/Logo') !!}
    @if($submitText == 'Alterar')
    @if($parceria->imagem)
    <img src="{{ url('assets/img/parcerias/'.$parceria->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('nome_pt', 'Nome (PT)') !!}
        {!! Form::text('nome_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('nome_en', 'Nome (EN)') !!}
        {!! Form::text('nome_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('nome_es', 'Nome (ES)') !!}
        {!! Form::text('nome_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_pt', 'Frase (PT)') !!}
        {!! Form::text('frase_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_en', 'Frase (EN)') !!}
        {!! Form::text('frase_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_es', 'Frase (ES)') !!}
        {!! Form::text('frase_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('parcerias.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>