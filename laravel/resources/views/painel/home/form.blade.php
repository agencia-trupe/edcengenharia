@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('capa', 'Capa Home - Imagem') !!}
    @if($home->capa)
    <img src="{{ url('assets/img/home/'.$home->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase1_capa_pt', 'Capa Home - Frase 1 (PT)') !!}
        {!! Form::text('frase1_capa_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase1_capa_en', 'Capa Home - Frase 1 (EN)') !!}
        {!! Form::text('frase1_capa_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase1_capa_es', 'Capa Home - Frase 1 (ES)') !!}
        {!! Form::text('frase1_capa_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase2_capa_pt', 'Capa Home - Frase 2 (PT)') !!}
        {!! Form::text('frase2_capa_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase2_capa_en', 'Capa Home - Frase 2 (EN)') !!}
        {!! Form::text('frase2_capa_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase2_capa_es', 'Capa Home - Frase 2 (ES)') !!}
        {!! Form::text('frase2_capa_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase3_capa_pt', 'Capa Home - Frase 3 (PT)') !!}
        {!! Form::text('frase3_capa_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase3_capa_en', 'Capa Home - Frase 3 (EN)') !!}
        {!! Form::text('frase3_capa_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase3_capa_es', 'Capa Home - Frase 3 (ES)') !!}
        {!! Form::text('frase3_capa_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_edc_pt', 'Capa Home - Frase EDC Eng. (PT)') !!}
        {!! Form::text('frase_edc_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_edc_en', 'Capa Home - Frase EDC Eng. (EN)') !!}
        {!! Form::text('frase_edc_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_edc_es', 'Capa Home - Frase EDC Eng. (ES)') !!}
        {!! Form::text('frase_edc_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos1_pt', 'Por que contratar 1 (PT)') !!}
        {!! Form::textarea('motivos1_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos1_en', 'Por que contratar 1 (EN)') !!}
        {!! Form::textarea('motivos1_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos1_es', 'Por que contratar 1 (ES)') !!}
        {!! Form::textarea('motivos1_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos2_pt', 'Por que contratar 2 (PT)') !!}
        {!! Form::textarea('motivos2_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos2_en', 'Por que contratar 2 (EN)') !!}
        {!! Form::textarea('motivos2_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos2_es', 'Por que contratar 2 (ES)') !!}
        {!! Form::textarea('motivos2_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos3_pt', 'Por que contratar 3 (PT)') !!}
        {!! Form::textarea('motivos3_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos3_en', 'Por que contratar 3 (EN)') !!}
        {!! Form::textarea('motivos3_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('motivos3_es', 'Por que contratar 3 (ES)') !!}
        {!! Form::textarea('motivos3_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_selo_pt', 'Frase Selo 11 anos (PT)') !!}
        {!! Form::textarea('frase_selo_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_selo_en', 'Frase Selo 11 anos (EN)') !!}
        {!! Form::textarea('frase_selo_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_selo_es', 'Frase Selo 11 anos (ES)') !!}
        {!! Form::textarea('frase_selo_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('imagem_vagas', 'Imagem Vagas') !!}
    @if($home->imagem_vagas)
    <img src="{{ url('assets/img/home/'.$home->imagem_vagas) }}" style="display:block; margin-bottom: 10px; width:auto; max-width: 100%;">
    @endif
    {!! Form::file('imagem_vagas', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_vagas_pt', 'Frase Vagas (PT)') !!}
        {!! Form::textarea('frase_vagas_pt', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_vagas_en', 'Frase Vagas (EN)') !!}
        {!! Form::textarea('frase_vagas_en', null, ['class' => 'form-control input-textarea']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_vagas_es', 'Frase Vagas (ES)') !!}
        {!! Form::textarea('frase_vagas_es', null, ['class' => 'form-control input-textarea']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('home.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>