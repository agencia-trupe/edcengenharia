@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">Empresas - Grupo EDC</h2>
</legend>


@if(!count($empresas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="empresas_grupo">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Logo</th>
                <th scope="col">Nome</th>
                <th scope="col">Serviços</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($empresas as $empresa)
            <tr id="{{ $empresa->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td style="background-color: #d5d4d4;"><img src="{{ asset('assets/img/empresas/'.$empresa->logo) }}" style="width: 100%; max-width:100px;" alt=""></td>
                <td>{{ $empresa->nome_pt }}</td>
                <td>{{ $empresa->link }}</td>
                <td class="crud-actions">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('empresas-grupo.edit', $empresa->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection