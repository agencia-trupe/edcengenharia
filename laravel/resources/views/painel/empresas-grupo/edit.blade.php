@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Empresas - Grupo EDC |</small> Editar Empresa</h2>
</legend>

{!! Form::model($empresa, [
'route' => ['empresas-grupo.update', $empresa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresas-grupo.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection