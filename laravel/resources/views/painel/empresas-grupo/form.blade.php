@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('logo', 'Logo') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresas/'.$empresa->imalogogem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('logo', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('nome_pt', 'Nome (PT)') !!}
        {!! Form::text('nome_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('nome_en', 'Nome (EN)') !!}
        {!! Form::text('nome_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('nome_es', 'Nome (ES)') !!}
        {!! Form::text('nome_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('servicos_pt', 'Serviços (3 principais) (PT)') !!}
        {!! Form::textarea('servicos_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('servicos_en', 'Serviços (3 principais) (EN)') !!}
        {!! Form::textarea('servicos_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('servicos_es', 'Serviços (3 principais) (ES)') !!}
        {!! Form::textarea('servicos_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <p class="obs"><strong>OBS:</strong> listar os <strong>3 principais serviços</strong> deixando 1 serviços por linha</p>
</div>

<div class="mb-3 col-12">
    {!! Form::label('link', 'Link Website') !!}
    {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('empresas-grupo.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>