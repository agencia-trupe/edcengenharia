@extends('frontend.layout.template')

@section('content')

<main class="home">

    <section class="banner">
        <img src="{{ asset('assets/img/home/'.$home->capa) }}" class="img-capa">
        <div class="textos-banner">
            <p class="frase1">{{ $home->{trans('database.frase1_capa')} }}</p>
            <p class="frase2">{{ $home->{trans('database.frase2_capa')} }}</p>
            <p class="frase3">{{ $home->{trans('database.frase3_capa')} }}</p>
            <div class="bg-frase-edc">
                <p class="frase-edc">{{ $home->{trans('database.frase_edc')} }}</p>
            </div>
        </div>
    </section>

    <section class="servicos">
        @foreach($servicos as $servico)
        @if(Lang::getLocale() == "en")
        <a href="{{ route('servicos-en', ['categoria_slug' => $servico->cat_slug_en, 'servico_slug' => $servico->slug_en]) }}" class="link-servico" style="background-image: url({{ asset('assets/img/servicos/'.$servico->capa) }})">
            <p class="categoria">{{ $servico->{trans('database.cat_titulo')} }}</p>
            <hr class="linha-divisao">
            <p class="servico">{{ $servico->{trans('database.titulo')} }}</p>
        </a>
        @elseif(Lang::getLocale() == "es")
        <a href="{{ route('servicos-es', ['categoria_slug' => $servico->cat_slug_es, 'servico_slug' => $servico->slug_es]) }}" class="link-servico" style="background-image: url({{ asset('assets/img/servicos/'.$servico->capa) }})">
            <p class="categoria">{{ $servico->{trans('database.cat_titulo')} }}</p>
            <hr class="linha-divisao">
            <p class="servico">{{ $servico->{trans('database.titulo')} }}</p>
        </a>
        @else
        <a href="{{ route('servicos', ['categoria_slug' => $servico->cat_slug_pt, 'servico_slug' => $servico->slug_pt]) }}" class="link-servico" style="background-image: url({{ asset('assets/img/servicos/'.$servico->capa) }})">
            <p class="categoria">{{ $servico->{trans('database.cat_titulo')} }}</p>
            <hr class="linha-divisao">
            <p class="servico">{{ $servico->{trans('database.titulo')} }}</p>
        </a>
        @endif
        @endforeach
    </section>

    <section class="contratar">
        <div class="center">
            <h2 class="titulo-verde">{{ trans('frontend.home.pq-contratar') }} <span>{{ $config->{trans('database.title')} }}</span> ?</h2>
            <div class="motivos">
                <article class="motivo">
                    <img src="{{ asset('assets/img/layout/icone-porquecontratar1.svg') }}" class="img-motivo" alt="">
                    <p class="frase-motivo">{{ $home->{trans('database.motivos1')} }}</p>
                </article>
                <article class="motivo">
                    <img src="{{ asset('assets/img/layout/icone-porquecontratar2.svg') }}" class="img-motivo" alt="">
                    <p class="frase-motivo">{{ $home->{trans('database.motivos2')} }}</p>
                </article>
                <article class="motivo">
                    <img src="{{ asset('assets/img/layout/icone-porquecontratar3.svg') }}" class="img-motivo" alt="">
                    <p class="frase-motivo">{{ $home->{trans('database.motivos3')} }}</p>
                </article>
            </div>
            <div class="informacoes">


                @if(app()->getLocale() === 'pt')
                <img src="{{ asset('assets/img/layout/selo-12anos-pt.svg') }}" class="img-selo" title="12 anos">
                @endif
                
                @if(app()->getLocale() === 'en')
                <img src="{{ asset('assets/img/layout/selo-12anos-eng.svg') }}" class="img-selo" title="12 years">
                @endif

                @if(app()->getLocale() === 'es')
                <img src="{{ asset('assets/img/layout/selo-12anos-esp.svg') }}" class="img-selo" title="12 anõs">
                @endif
               




                <p class="frase-selo">{{ $home->{trans('database.frase_selo')} }}</p>
                <img src="{{ asset('assets/img/layout/marca-edc-group-slogan.svg') }}" class="img-edc-slogan" title="EDC Group">
                <div class="sobre-edc">
                    <p class="itens-edc">{{ trans('frontend.home.edc-menus') }}</p>
                    <div>
                        <a href="https://www.edcgroup.com.br/" class="link-edc" alt="Site EDC Group">{{ trans('frontend.home.visite-site') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="parcerias">
        <h2 class="titulo-verde">{{ trans('frontend.home.parcerias') }}</h2>
        <div class="center">
            @foreach($parcerias as $parceria)
            <div class="parceria">
                <img src="{{ asset('assets/img/parcerias/'.$parceria->imagem) }}" class="img-parceria" title="{{ $parceria->{trans('database.nome')} }}">
                <p class="nome-parceria">{{ $parceria->{trans('database.nome')} }}</p>
                <p class="frase-parceria">{{ $parceria->{trans('database.frase')} }}</p>
                <a href="{{ $parceria->link }}" target="_blank" class="link-parceria">{{ trans('frontend.home.visite-website') }}</a>
            </div>
            @endforeach
        </div>
    </section>

    <section class="vagas">
        <div class="center">
            <img src="{{ asset('assets/img/home/'.$home->imagem_vagas) }}" class="img-vagas" title="{{ trans('frontend.home.vagas') }}">
            <aside class="sobre-vagas">
                <h1 class="titulo-vagas">
                    {{ trans('frontend.home.vagas') }}
                    <img src="{{ asset('assets/img/layout/seta-losango.svg') }}" class="img-seta" title="{{ trans('frontend.home.vagas') }}">
                </h1>
                <p class="frase-vagas">{{ $home->{trans('database.frase_vagas')} }}</p>
                <div>
                    <a href="{{ $contato->link_vagas }}" class="link-vagas" alt="">{{ trans('frontend.home.link-vagas') }}</a>
                </div>
            </aside>
        </div>
    </section>

    <section class="grupo-edc">
        <div class="center">
            <p class="frase-grupo">{{ trans('frontend.home.frase-grupo') }}</p>
            <article class="grupo">
                @foreach($empresas as $empresa)
                <a href="{{ $empresa->link }}" target="_blank" class="@if($empresa->id == 1) edc-servicos @elseif($empresa->id == 2) edc-engenharia @else edc-uni @endif">
                    <img src="{{ asset('assets/img/empresas/'.$empresa->logo) }}" class="img-logo" title="{{ $empresa->{trans('database.nome')} }}">                   
                    @if($empresa->nome_pt == 'EDC Smart')
                    <div class="servicos">
                        {!! $empresa->{trans('database.servicos')} !!}
                        <hr class="linha-divisao l1">
                    </div>
                    @else
                    <div class="servicos">
                        {!! $empresa->{trans('database.servicos')} !!}
                        <hr class="linha-divisao l1">
                        <hr class="linha-divisao l2">
                    </div>
                    @endif
                    <p class="@if($empresa->id == 1) link-servicos @elseif($empresa->id == 2) link-engenharia @else link-uni @endif">
                        {{ trans('frontend.home.visite-website') }}
                    </p>
                </a>
                @endforeach
            </article>
        </div>
    </section>

</main>

@endsection