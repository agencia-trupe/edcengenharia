@extends('frontend.layout.template')

@section('content')

<main class="contrate">

    <section class="informacoes">

        <article class="left">
            <div class="contatos">
                <img src="{{ asset('assets/img/layout/seta-marcador.svg') }}" class="img-marcador" alt="">
                <h2 class="titulo-pagina">{{ trans('frontend.contrate-titulo') }}</h2>
                <div class="dados" style="background-image: url({{ asset('assets/img/contatos/'.$contato->imagem) }})">
                    @php
                    $telefone = "+55".str_replace(" ", "", $contato->telefone);
                    $whatsapp = "55".str_replace(" ", "", $contato->whatsapp);
                    @endphp
                    <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-telefone" target="_blank">+55 {{ $contato->whatsapp }}</a>
                    <a href="tel:+55{{ $contato->telefone }}" class="link-telefone">+55 {{ $contato->telefone }}</a>
                    <p class="atendimento">{{ $contato->{trans('database.atendimento')} }}</p>
                </div>
            </div>
            <div class="edc-group">
                <img src="{{ asset('assets/img/layout/marca-edc-group.svg') }}" alt="EDC GROUP" class="img-edc-group">
                <div class="endereco-completo">
                    <p class="endereco">{{ $contato->{trans('database.endereco_pt1')} }}</p>
                    <p class="endereco">{{ $contato->{trans('database.endereco_pt2')} }}</p>
                </div>
            </div>
        </article>

        <article class="right">
            <p class="frase">{{ $contato->{trans('database.frase')} }}</p>
            <form action="{{ route('contrate.post') }}" method="POST" enctype="multipart/form-data" class="form-contrate">
                {!! csrf_field() !!}
                <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" class="input-telefone" value="{{ old('telefone') }}">
                
                <input type="text" name="empresa" placeholder="{{ trans('frontend.contato.empresa') }}" value="" required>
                <input type="text" name="cargo" placeholder="{{ trans('frontend.contato.cargo') }}" value="" required>

                
                    
                    <select name="interesse" id="" class='contato-interesse' style="width: 100%;margin-bottom: 5px;height: 35px;font-family: 'Open Sans';color:#606062;padding-left: 5px">
                        <option value="#">{{ trans('frontend.contato.interesse') }} (Selecione)</option>
                        <option value="Terceiros e Temporários">Terceiros e Temporários</option>
                        <option value="Outsourcing Especializado">Outsourcing Especializado e BPO</option>
                        <option value="Pessoas & Estratégia">Hunting</option>
                        <option value="Outros">outros</option>
                    </select>
               

                <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar"></button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif

                @if(session('enviado'))
                <div class="flash flash-sucesso">
                    <p>{{ trans('frontend.contato.msg-sucesso') }}</p>
                </div>
                @endif
            </form>

            <a href="https://www.edcgroup.com.br/candidato" target="_blank">
                <div class="work_for_us">
                    <h1>TRABALHE CONOSCO</h1>
                    <P>Para consultar vagas e enviar seu curriculo acesse a <strong>ÁREA DO CANDIDATO EDC</strong></P>
                </div>
            </a>
            
        </article>
    </section>

    <section class="mapa">
        {!! $contato->google_maps !!}
    </section>

</main>

@endsection