@extends('frontend.layout.template')

@section('content')

<main class="servicos">

    <section class="servico">
        <article class="left">
            <div class="dados">
                <img src="{{ asset('assets/img/layout/seta-marcador.svg') }}" class="img-marcador" alt="">
                <div class="textos">
                    <h2 class="titulo-categoria">{{ $categoria->{trans('database.titulo')} }}</h2>
                    <hr class="linha-titulos">
                    <h2 class="titulo-servico">{{ $servico->{trans('database.titulo')} }}</h2>
                </div>
            </div>
            <div class="imagem">
                <img src="{{ asset('assets/img/servicos/'.$servico->{trans('database.imagem')}) }}" class="img-servico">
            </div>
        </article>

        <article class="right">
            
            <img src="{{ asset('assets/img/servicos/'.$servico->capa) }}" class="img-capa">

            {{-- é isto aqui o texto --}}
            <div class="t">{!! $servico->{trans('database.texto')} !!}</div>
        </article>
        
        <div class="imagem-mobile">
            <img src="{{ asset('assets/img/servicos/'.$servico->{trans('database.imagem')}) }}" class="img-servico">
        </div>
    </section>

    <section class="mais-infos">
        <div class="center">
            <p class="frase">
                
                {{ trans('frontend.mais-informacoes') }}

                <img src="{{ asset('assets/img/layout/seta-maisinfo.svg') }}" class="img-seta" title="Mais Informações">
            </p>
            <form action="{{ route('servicos.post', ['categoria_slug' => $categoria->{trans('database.slug')}, 'servico_slug' => $servico->{trans('database.slug')}]) }}" method="POST" enctype="multipart/form-data" class="form-servicos">
                {!! csrf_field() !!}
                <div class="dados">
                    <input type="hidden" name="servico_id" value="{{ $servico->id }}">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    
                   
                    
                    <input type="text" name="telefone" placeholder="telefone" class="input-telefone" value="{{ old('telefone') }}">


                    <input type="hidden" name='origem' value='Serviços'>

                   {{--  <input type="text" name="empresa" placeholder="empresa" required>
                    <input type="text" name="cargo" placeholder="cargo" required>
                    <select name="interesse" id=""  style='padding:7px 0px 7px 15px;font-size:14px;width:100%; 
                    border:1px solid #eaeee7; background:#eaeee7;color:#a2a9bd;margin:0px 0px 5px 0px;'>
                        <option value="#">Interesse (Selecione)</option>
                        <option value="Outsourcing Especializado">Outsourcing Especializado</option>
                        <option value="BPO">BPO</option>
                        <option value="Projetos Especiais">Projetos Especiais</option>
                        <option value="Outros">Outros</option>
                    </select> --}}

                </div>
              {{--   <textarea name="mensagem" placeholder="mensagem" style='height:283px;' required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar" style='height:283px;'>
                    <img src="{{ asset('assets/img/layout/icone-enviar.svg') }}" class="img-enviar" title="Enviar">
                </button> --}}

                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar" >
                    <img src="{{ asset('assets/img/layout/icone-enviar.svg') }}" class="img-enviar" title="Enviar">
                </button>
            </form>
        </div>
        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            <p>Mensagem enviada com sucesso!</p>
        </div>
        @endif
    </section>

</main>

@endsection