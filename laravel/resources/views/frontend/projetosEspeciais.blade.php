@extends('frontend.layout.pe_template')

@section('content')

<main class="categorias">

    <section class="categoria">
        <article class="sobre">
            <img src="{{ asset('assets/img/layout/seta-marcador.svg') }}" class="img-marcador" alt="">
            <div class="textos">
                <h2 class="titulo-categoria">{{ $categoria->{trans('database.titulo')} }}</h2>
                <div class="texto-categoria">{!! $categoria->{trans('database.texto')} !!}</div>
            </div>
        </article>
        <article class="servicos">
            @foreach($servicos as $servico)
            @if(Lang::getLocale() == "en")
            <a href="{{ route('servicos-en', ['categoria_slug' => $categoria->slug_en, 'servico_slug' => $servico->slug_en]) }}" class="link-servico">
                <img src="{{ asset('assets/img/servicos/mini/'.$servico->capa) }}" class="img-capa">
                <p class="categoria">{{ $categoria->{trans('database.titulo')} }}</p>
                <hr class="linha-divisao">
                <p class="servico">{{ $servico->{trans('database.titulo')} }}</p>
            </a>
            @elseif(Lang::getLocale() == "es")
            <a href="{{ route('servicos-es', ['categoria_slug' => $categoria->slug_es, 'servico_slug' => $servico->slug_es]) }}" class="link-servico">
                <img src="{{ asset('assets/img/servicos/mini/'.$servico->capa) }}" class="img-capa">
                <p class="categoria">{{ $categoria->{trans('database.titulo')} }}</p>
                <hr class="linha-divisao">
                <p class="servico">{{ $servico->{trans('database.titulo')} }}</p>
            </a>
            @else
            <a href="{{ route('servicos', ['categoria_slug' => $categoria->slug_pt, 'servico_slug' => $servico->slug_pt]) }}" class="link-servico">
                <img src="{{ asset('assets/img/servicos/mini/'.$servico->capa) }}" class="img-capa">
                <p class="categoria">{{ $categoria->{trans('database.titulo')} }}</p>
                <hr class="linha-divisao">
                <p class="servico">{{ $servico->{trans('database.titulo')} }}</p>
            </a>
            @endif
            @endforeach
        </article>
    </section>

    <section class="vantagens">
        <h2 class="titulo-vantagens">{{ trans('frontend.vantagens-titulo') }}</h2>
        <div class="center">
            <article class="vantagem">
                <img src="{{ asset('assets/img/categorias/'.$categoria->vantagem1_icone) }}" class="img-vantagem" alt="">
                
                <p class="titulo-vantagem">{{ $categoria->{trans('database.vantagem1_titulo')} }}</p>

                <div class="texto-vantagem">{!! $categoria->{trans('database.vantagem1_texto')} !!}</div>
            </article>
            <article class="vantagem">
                <img src="{{ asset('assets/img/categorias/'.$categoria->vantagem2_icone) }}" class="img-vantagem" alt="">
                <p class="titulo-vantagem">{{ $categoria->{trans('database.vantagem2_titulo')} }}</p>
                <div class="texto-vantagem">{!! $categoria->{trans('database.vantagem2_texto')} !!}</div>
            </article>
            <article class="vantagem">
                <img src="{{ asset('assets/img/categorias/'.$categoria->vantagem3_icone) }}" class="img-vantagem" alt="">
                <p class="titulo-vantagem">{{ $categoria->{trans('database.vantagem3_titulo')} }}</p>
                <div class="texto-vantagem">{!! $categoria->{trans('database.vantagem3_texto')} !!}</div>
            </article>
        </div>
    </section>

</main>

@endsection