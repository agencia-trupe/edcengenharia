@foreach($categorias as $categoria)
@if(Lang::getLocale() == "en")
<a href="{{ route('categorias-en', ['slug' => $categoria->slug_en]) }}" class="link-categoria @if(url()->current() == route('categorias-en', $categoria->slug_en)) active @endif">
    {{ $categoria->{trans('database.titulo')} }}
</a>
@elseif(Lang::getLocale() == "es")
<a href="{{ route('categorias-es', ['slug' => $categoria->slug_es]) }}" class="link-categoria @if(url()->current() == route('categorias-es', $categoria->slug_es)) active @endif">
    {{ $categoria->{trans('database.titulo')} }}
</a>
@else
<a href="{{ route('categorias', ['slug' => $categoria->slug_pt]) }}" class="link-categoria @if(url()->current() == route('categorias', $categoria->slug_pt)) active @endif">
    {{ $categoria->{trans('database.titulo')} }}
</a>
@endif
@endforeach

@if(Lang::getLocale() == "en")
<a href="{{ route('contrate-en') }}" class="link-contrate @if(Tools::routeIs('contrate-en')) active @endif">
    {{ trans('frontend.geral.contrate') }}
</a>
@elseif(Lang::getLocale() == "es")
<a href="{{ route('contrate-es') }}" class="link-contrate @if(Tools::routeIs('contrate-es')) active @endif">
    {{ trans('frontend.geral.contrate') }}
</a>
@else
<a href="{{ route('contrate') }}" class="link-contrate @if(Tools::routeIs('contrate')) active @endif">
    {{ trans('frontend.geral.contrate') }}
</a>
@endif