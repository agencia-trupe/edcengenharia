import MobileToggle from "./MobileToggle";

MobileToggle();

$(document).ready(function () {
    // LANG
    $("select[name=lang]").change(function selectRoute() {
        window.location = $(this)
            .find(`option[value=${this.value}]`)
            .data("route");
    });

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // CATEGORIAS - removendo margin-right dos 2ºs elementos
    $("main.categorias .link-servico:nth-child(2n)").css("margin-right", "0");

    // Whatsapp floating
    $("#chatWhatsapp").floatingWhatsApp({
        phone: "+55" + whatsappNumero,
        popupMessage:
            "Seja muito bem-vindo(a) à EDC Engenharia. Como podemos ajudar?",
        showPopup: true,
        autoOpen: false,
        headerTitle:
            '<div class="img-edc-uni"><img src="' +
            imgMarcaWhatsapp +
            '" alt=""></div>' +
            '<div class="textos"><p class="titulo">EDC Engenharia</p><p class="frase">Responderemos o mais breve possível.</p></div>',
        headerColor: "#25D366",
        size: "60px",
        position: "right",
        linkButton: true,
    });

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
