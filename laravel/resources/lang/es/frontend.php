<?php

return [
    '404' => 'Página no encontrada',

    'geral' => [
        'vagas'             => 'Vacantes',
        'vagas-texto'       => 'Ven a trabajar con nosotros. Consulta nuestras vacantes',
        'area-do-candidato' => 'Área del candidato',
        'area-do-consultor' => 'Área del consultor',
        'contrate'          => 'CONTRATAR',
        'termos'            => 'Terminos de uso',
        'politica'          => 'Política de privacidad',
        'direitos'          => 'Todos los derechos reservados',
        'criacao'           => 'Creación de sitios web: ',
        'edc-group'         => 'Es una empresa del Grupo EDC',
        'titulo-forms' => 'HABLA CON UN ESPECIALISTA',
    ],

    'home' => [
        'pq-contratar'   => 'POR QUÉ CONTRATAR',
        'edc-menus'      => 'Histórico • Estructura • Equipo • Noticias',
        'visite-site'    => 'visite el sitio web del Grupo EDC »',
        'parcerias'      => 'ASOCIACIONES ESTRATÉGICAS',
        'vagas'          => 'VACANTES',
        'link-vagas'     => 'Buscar vacantes y registrarse »',
        'frase-grupo'    => 'EDC SERVIÇOS ES UNA EMPRESA DEL GRUPO EDC • DESCUBRE LAS EMPRESAS DEL GRUPO:',
        'edc-servicos'   => 'Servicios EDC',
        'visite-website' => 'VISITA EL SITIO WEB',
        'edc-engenharia' => 'Ingeniería EDC',
        'edc-uni'        => 'EDC UNI',
    ],

    'contrate-titulo'  => 'CONTRATAR CON NOSOTROS',
    'vantagens-titulo' => 'BENEFICIOS',
    'mais-informacoes' => 'PIDA MAS INFORMACIÓN',

    'contato' => [
        'nome'        => 'nombre',
        'telefone'    => 'teléfono',
        'mensagem'    => 'mensaje',
        'empresa'    => 'empresa',
        'cargo'    => 'oficina',
        'interesse'    => 'interés',
        'msg-sucesso' => 'Mensaje enviado correctamente!',
    ],

    'cookies1' => 'Usamos cookies para personalizar contenido, rastrear anuncios y brindarle una experiencia de navegación más segura. Si continúa navegando en nuestro sitio web, acepta nuestro uso de esta información. Lee nuestro ',
    'cookies2' => 'Política de privacidad',
    'cookies3' => ' y aprenda más.',
    'btn-cookies' => 'ACEPTAR Y CERRAR',
];
