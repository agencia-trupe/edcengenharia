<?php

return [
    '404' => 'Page not found',

    'geral' => [
        'vagas'             => 'Jobs',
        'vagas-texto'       => 'Work with us. Search available jobs',
        'area-do-candidato' => 'Candidate Area',
        'area-do-consultor' => 'Consultant Area',
        'contrate'          => 'HIRE',
        'termos'            => 'Terms of use',
        'politica'          => 'Privacy Policy',
        'direitos'          => 'All rights reserved',
        'criacao'           => 'Website creation: ',
        'edc-group'         => 'It is an EDC Group company',
        'titulo-forms' => 'TALK TO A SPECIALIST',
    ],

    'home' => [
        'pq-contratar'   => 'WHY CONTRACT',
        'edc-menus'      => 'Historic • Structure • Team • News',
        'visite-site'    => 'visit the EDC Group website »',
        'parcerias'      => 'STRATEGIC PARTNERSHIPS',
        'vagas'          => 'JOBS',
        'link-vagas'     => 'Search for vacancies and register »',
        'frase-grupo'    => 'EDC SERVICES IS AN EDC GROUP COMPANY • DISCOVER THE GROUP COMPANIES:',
        'edc-servicos'   => 'EDC Services',
        'visite-website' => 'VISIT THE WEBSITE',
        'edc-engenharia' => 'EDC Engineering',
        'edc-uni'        => 'EDC UNI',
    ],

    'contrate-titulo'  => 'CONTRACT WITH US',
    'vantagens-titulo' => 'BENEFITS',
    'mais-informacoes' => 'REQUEST MORE INFORMATION',

    'contato' => [
        'nome'        => 'name',
        'telefone'    => 'telephone',
        'mensagem'    => 'message',
        'empresa'    => 'company',
        'cargo'    => 'office',
        'interesse'    => 'interest',
        'msg-sucesso' => 'Message sent successfully!',
    ],

    'cookies1' => 'We use cookies to personalize content, track ads and provide a safer browsing experience for you. By continuing to browse our website, you agree to our use of this information. Read our ',
    'cookies2' => 'Privacy Policy',
    'cookies3' => ' and learn more.',
    'btn-cookies' => 'ACCEPT AND CLOSE',
];
