<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('categoria_id')->constrained('categorias')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('capa');
            $table->string('slug_pt');
            $table->string('slug_en')->nullable();
            $table->string('slug_es')->nullable();
            $table->string('titulo_pt');
            $table->string('titulo_en')->nullable();
            $table->string('titulo_es')->nullable();
            $table->text('texto_pt');
            $table->text('texto_en')->nullable();
            $table->text('texto_es')->nullable();
            $table->string('imagem_pt');
            $table->string('imagem_en')->nullable();
            $table->string('imagem_es')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
