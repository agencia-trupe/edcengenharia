<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParceriasTable extends Migration
{
    public function up()
    {
        Schema::create('parcerias', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('nome_pt');
            $table->string('nome_en')->nullable();
            $table->string('nome_es')->nullable();
            $table->string('frase_pt');
            $table->string('frase_en')->nullable();
            $table->string('frase_es')->nullable();
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('parcerias');
    }
}
