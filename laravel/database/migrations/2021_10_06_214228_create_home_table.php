<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('capa');
            $table->string('frase1_capa_pt');
            $table->string('frase1_capa_en')->nullable();
            $table->string('frase1_capa_es')->nullable();
            $table->string('frase2_capa_pt');
            $table->string('frase2_capa_en')->nullable();
            $table->string('frase2_capa_es')->nullable();
            $table->string('frase3_capa_pt');
            $table->string('frase3_capa_en')->nullable();
            $table->string('frase3_capa_es')->nullable();
            $table->string('frase_edc_pt');
            $table->string('frase_edc_en')->nullable();
            $table->string('frase_edc_es')->nullable();
            $table->string('motivos1_pt');
            $table->string('motivos1_en')->nullable();
            $table->string('motivos1_es')->nullable();
            $table->string('motivos2_pt');
            $table->string('motivos2_en')->nullable();
            $table->string('motivos2_es')->nullable();
            $table->string('motivos3_pt');
            $table->string('motivos3_en')->nullable();
            $table->string('motivos3_es')->nullable();
            $table->string('frase_selo_pt');
            $table->string('frase_selo_en')->nullable();
            $table->string('frase_selo_es')->nullable();
            $table->string('imagem_vagas');
            $table->string('frase_vagas_pt');
            $table->string('frase_vagas_en')->nullable();
            $table->string('frase_vagas_es')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('home');
    }
}
