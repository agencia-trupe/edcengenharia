<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->id();
            $table->string('slug_pt');
            $table->string('slug_en')->nullable();
            $table->string('slug_es')->nullable();
            $table->string('titulo_pt');
            $table->string('titulo_en')->nullable();
            $table->string('titulo_es')->nullable();
            $table->text('texto_pt');
            $table->text('texto_en')->nullable();
            $table->text('texto_es')->nullable();
            $table->string('vantagem1_icone');
            $table->string('vantagem1_titulo_pt');
            $table->string('vantagem1_titulo_en')->nullable();
            $table->string('vantagem1_titulo_es')->nullable();
            $table->text('vantagem1_texto_pt');
            $table->text('vantagem1_texto_en')->nullable();
            $table->text('vantagem1_texto_es')->nullable();
            $table->string('vantagem2_icone');
            $table->string('vantagem2_titulo_pt');
            $table->string('vantagem2_titulo_en')->nullable();
            $table->string('vantagem2_titulo_es')->nullable();
            $table->text('vantagem2_texto_pt');
            $table->text('vantagem2_texto_en')->nullable();
            $table->text('vantagem2_texto_es')->nullable();
            $table->string('vantagem3_icone');
            $table->string('vantagem3_titulo_pt');
            $table->string('vantagem3_titulo_en')->nullable();
            $table->string('vantagem3_titulo_es')->nullable();
            $table->text('vantagem3_texto_pt');
            $table->text('vantagem3_texto_en')->nullable();
            $table->text('vantagem3_texto_es')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
