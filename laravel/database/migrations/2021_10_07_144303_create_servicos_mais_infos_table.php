<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosMaisInfosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos_mais_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('servico_id')->constrained('servicos')->onDelete('cascade');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone')->nullable();
            $table->text('mensagem');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('servicos_mais_infos');
    }
}
