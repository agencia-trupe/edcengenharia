<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title_pt'                   => 'EDC ENGENHARIA',
            'title_en'                   => 'EDC ENGINEERING',
            'title_es'                   => 'INGENIERÍA EDC',
            'description_pt'             => '',
            'description_en'             => '',
            'description_es'             => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics_ua'               => '',
            'analytics_g'                => '',
            'codigo_gtm'                 => '',
            'pixel_facebook'             => '',
            'tinify_key'                 => '',
        ]);
    }
}
