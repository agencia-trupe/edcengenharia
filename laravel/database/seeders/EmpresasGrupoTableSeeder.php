<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresasGrupoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresas_grupo')->insert([
            'id'          => 1,
            'ordem'       => 1,
            'logo'        => 'marca-edc-servicos-branco_202202022012514Vrl8EiWqq.png',
            'nome_pt'     => 'EDC Serviços',
            'nome_en'     => 'EDC Services',
            'nome_es'     => 'Servicios EDC',
            'servicos_pt' => '<p>TERCEIRIZAÇÃO</p><p>MÃO DE OBRA TEMPORÁRIA</p><p>HEAD HUNTING</p>',
            'servicos_en' => '<p>OUTSOURCING</p><p>TEMPORARY EMPLOYMENT</p><p>HEAD HUNTING</p>',
            'servicos_es' => '<p>SUBCONTRATACIÓN</p><p>EMPLEO TEMPORAL</p><p>HEAD HUNTING</p>',
            'link'        => 'https://edcservicos.com.br/',
        ]);

        DB::table('empresas_grupo')->insert([
            'id'          => 2,
            'ordem'       => 2,
            'logo'        => 'marca-edc-engenharia-branco_20220202202546jlf4JFqz58.png',
            'nome_pt'     => 'EDC Engenharia',
            'nome_en'     => 'EDC Engineering',
            'nome_es'     => 'Ingeniería EDC',
            'servicos_pt' => '<p>OUTSOURCING ESPECIALIZADO</p><p>BPO (Business Process Outsourcing)</p><p>PROJETOS ESPECIAIS</p>',
            'servicos_en' => '<p>SPECIALIZED OUTSOURCING</p><p>BPO (Business Process Outsourcing)</p><p>SPECIAL PROJECTS</p>',
            'servicos_es' => '<p>TERCERIZACIÓN ESPECIALIZADA</p><p>BPO (Business Process Outsourcing)</p><p>PROYECTOS ESPECIALES</p>',
            'link'        => 'http://www.edcengenharia.com.br/',
        ]);

        DB::table('empresas_grupo')->insert([
            'id'          => 3,
            'ordem'       => 3,
            'logo'        => 'marca-edc-uni-branco_202202022026551FWOOpDXHf.png',
            'nome_pt'     => 'EDC UNI',
            'nome_en'     => 'EDC UNI',
            'nome_es'     => 'EDC UNI',
            'servicos_pt' => '<p>CONSULTORIA ESTRATÉGICA RH</p><p>DESENVOLVIMENTO DE PESSOAS</p><p>HUNTING ESPECIALISTA</p>',
            'servicos_en' => '<p>HR STRATEGIC CONSULTING</p><p>DEVELOPING PEOPLE</p><p>SPECIALIST HUNTING</p>',
            'servicos_es' => '<p>CONSULTORÍA ESTRATÉGICA DE RRHH</p><p>PERSONAS EN DESARROLLO</p><p>CAZA ESPECIALIZADA</p>',
            'link'        => 'https://www.edcuni.com.br/',
        ]);
    }
}
