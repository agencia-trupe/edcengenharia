<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicosMaisInfosRequest;
use App\Models\Categoria;
use App\Models\Contato;
use App\Models\Servico;
use App\Models\ServicoMaisInfo;
use App\Notifications\ServicosMaisInfosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ServicosController extends Controller
{
    public function index($categoria_slug, $servico_slug)
    {
        $categoria = Categoria::where('slug_pt', $categoria_slug)->first();
        if ($categoria == null) {
            $categoria = Categoria::where('slug_en', $categoria_slug)->first();
            if ($categoria == null) {
                $categoria = Categoria::where('slug_es', $categoria_slug)->first();
            }
        }

        $servico = Servico::categoria($categoria->id)->where('slug_pt', $servico_slug)->first();
        if ($servico == null) {
            $servico = Servico::categoria($categoria->id)->where('slug_en', $servico_slug)->first();
            if ($categoria == null) {
                $servico = Servico::categoria($categoria->id)->where('slug_es', $servico_slug)->first();
            }
        }

        return view('frontend.servicos', compact('categoria', 'servico'));
    }

    public function formPost(ServicosMaisInfosRequest $request, ServicoMaisInfo $maisInfosServico, $categoria_slug, $servico_slug)
    {
        try {
            $data = $request->all();

            $maisInfosServico->create($data);

            $servico = Servico::where('id', $data['servico_id'])->first();
            $categoria = Categoria::where('id', $servico->categoria_id)->first();

            $data['servico_titulo'] = $servico->titulo_pt;
            $data['categoria_titulo'] = $categoria->titulo_pt;

            Notification::send(
                Contato::first(),
                new ServicosMaisInfosNotification($data)
            );

            return redirect()->back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }
}
