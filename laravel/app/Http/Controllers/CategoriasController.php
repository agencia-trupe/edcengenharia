<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Servico;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{



    public function oe()
    {
        $categoria = Categoria::find(1);
        $servicos = Servico::categoria($categoria->id)->ordenados()->get();
        return view('frontend.outsourcingEspecializado', ['categoria' => $categoria, 'servicos' => $servicos]);
    }

    public function bpo()
    {
        $categoria = Categoria::find(2);
        $servicos = Servico::categoria($categoria->id)->ordenados()->get();
        return view('frontend.bpo', ['categoria' => $categoria, 'servicos' => $servicos]);
    }

    public function pe()
    {
        $categoria = Categoria::find(3);
        $servicos = Servico::categoria($categoria->id)->ordenados()->get();
        return view('frontend.projetosEspeciais', ['categoria' => $categoria, 'servicos' => $servicos]);
    }

    public function index($slug)
    {
        $categoria = Categoria::where('slug_pt', $slug)->first();
        if ($categoria == null) {
            $categoria = Categoria::where('slug_en', $slug)->first();
            if ($categoria == null) {
                $categoria = Categoria::where('slug_es', $slug)->first();
            }
        }

        $servicos = Servico::categoria($categoria->id)->ordenados()->get();
        // dd($servicos);

        return view('frontend.categorias', compact('categoria', 'servicos'));
    }




}
