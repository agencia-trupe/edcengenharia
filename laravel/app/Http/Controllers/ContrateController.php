<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContrateConoscoRequest;
use App\Models\Contato;
use App\Models\ContrateConosco;
use App\Notifications\ContrateConoscoNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContrateController extends Controller
{
    public function index()
    {
        return view('frontend.contrate');
    }

    public function formPost(ContrateConoscoRequest $request, ContrateConosco $contatoContrate)
    {
        try {
            $data = $request->all();

            $contatoContrate->create($data);

            Notification::send(
                Contato::first(),
                new ContrateConoscoNotification($data)
            );


            session()->flash('success','Mensagem Enviada com Sucesso!');

            return redirect('contrate')->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }
}
