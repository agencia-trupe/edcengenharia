<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\EmpresaGrupo;
use App\Models\Home;
use App\Models\Parceria;
use App\Models\Servico;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();

        $servicos = Servico::join('categorias', 'categorias.id', '=', 'servicos.categoria_id')
            ->select('categorias.slug_pt as cat_slug_pt', 'categorias.slug_en as cat_slug_en', 'categorias.slug_es as cat_slug_es', 'categorias.titulo_pt as cat_titulo_pt', 'categorias.titulo_en as cat_titulo_en', 'categorias.titulo_es as cat_titulo_es', 'servicos.*')
            ->orderBy('servicos.categoria_id', 'asc')->get();

        $parcerias = Parceria::ordenados()->get();

        $empresas = EmpresaGrupo::ordenados()->get();

        return view('frontend.home', compact('home', 'servicos', 'parcerias', 'empresas'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
