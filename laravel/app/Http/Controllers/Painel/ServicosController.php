<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosRequest;
use App\Models\Categoria;
use App\Models\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServicosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('id', 'asc')->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $servicos = Servico::categoria($categoria)
                ->join('categorias', 'categorias.id', '=', 'servicos.categoria_id')
                ->select('categorias.titulo_pt as categoria_titulo', 'servicos.*')
                ->ordenados()->get();
        } else {
            $servicos = Servico::join('categorias', 'categorias.id', '=', 'servicos.categoria_id')
                ->select('categorias.titulo_pt as categoria_titulo', 'servicos.*')
                ->ordenados()->get();
        }

        return view('painel.servicos.index', compact('servicos', 'categorias'));
    }

    public function create()
    {
        $categorias = Categoria::orderBy('id', 'asc')->pluck('titulo_pt', 'id');

        return view('painel.servicos.create', compact('categorias'));
    }

    public function store(ServicosRequest $request)
    {
        try {
            $input = $request->all();

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) $input['slug_en'] = Str::slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = Str::slug($request->titulo_es, "-");

            if (isset($input['capa'])) $input['capa'] = Servico::upload_capa();
            if (isset($input['imagem_pt'])) $input['imagem_pt'] = Servico::upload_imagem_pt();
            if (isset($input['imagem_en'])) $input['imagem_en'] = Servico::upload_imagem_en();
            if (isset($input['imagem_es'])) $input['imagem_es'] = Servico::upload_imagem_es();

            Servico::create($input);

            return redirect()->route('servicos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Servico $servico)
    {
        $categorias = Categoria::orderBy('id', 'asc')->pluck('titulo_pt', 'id');

        return view('painel.servicos.edit', compact('servico', 'categorias'));
    }

    public function update(ServicosRequest $request, Servico $servico)
    {
        try {
            $input = $request->all();

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) $input['slug_en'] = Str::slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = Str::slug($request->titulo_es, "-");

            if (isset($input['capa'])) $input['capa'] = Servico::upload_capa();
            if (isset($input['imagem_pt'])) $input['imagem_pt'] = Servico::upload_imagem_pt();
            if (isset($input['imagem_en'])) $input['imagem_en'] = Servico::upload_imagem_en();
            if (isset($input['imagem_es'])) $input['imagem_es'] = Servico::upload_imagem_es();

            $servico->update($input);

            return redirect()->route('servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Servico $servico)
    {
        try {
            $servico->delete();

            return redirect()->route('servicos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
