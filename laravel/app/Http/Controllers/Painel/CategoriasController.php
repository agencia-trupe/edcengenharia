<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasRequest;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoriasController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('id', 'asc')->get();

        return view('painel.categorias.index', compact('categorias'));
    }

    public function edit(Categoria $categoria)
    {
        return view('painel.categorias.edit', compact('categoria'));
    }

    public function update(CategoriasRequest $request, Categoria $categoria)
    {
        try {
            $input = $request->all();

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) $input['slug_en'] = Str::slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = Str::slug($request->titulo_es, "-");

            if (isset($input['vantagem1_icone'])) $input['vantagem1_icone'] = Categoria::upload_icone1();
            if (isset($input['vantagem2_icone'])) $input['vantagem2_icone'] = Categoria::upload_icone2();
            if (isset($input['vantagem3_icone'])) $input['vantagem3_icone'] = Categoria::upload_icone3();

            $categoria->update($input);

            return redirect()->route('categorias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
