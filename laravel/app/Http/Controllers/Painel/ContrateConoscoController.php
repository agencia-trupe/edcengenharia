<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\ContrateConosco;
use Illuminate\Http\Request;

class ContrateConoscoController extends Controller
{
    public function index()
    {
        $contatos = ContrateConosco::orderBy('created_at', 'DESC')->get();

        return view('painel.contatos.contrate-conosco.index', compact('contatos'));
    }

    public function show($contato)
    {
        $contatoContrate = ContrateConosco::where('id', $contato)->first();
        $contatoContrate->update(['lido' => 1]);

        return view('painel.contatos.contrate-conosco.show', compact('contatoContrate'));
    }

    public function destroy($contato)
    {
        try {
            $contatoContrate = ContrateConosco::where('id', $contato)->first();
            $contatoContrate->delete();

            return redirect()->route('contrate-conosco.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($contato)
    {
        try {
            $contatoContrate = ContrateConosco::where('id', $contato)->first();
            $contatoContrate->update([
                'lido' => !$contatoContrate->lido
            ]);

            return redirect()->route('contrate-conosco.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
