<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ParceriasRequest;
use App\Models\Parceria;
use Illuminate\Http\Request;

class ParceriasController extends Controller
{
    public function index()
    {
        $parcerias = Parceria::ordenados()->get();

        return view('painel.parcerias.index', compact('parcerias'));
    }

    public function create()
    {
        return view('painel.parcerias.create');
    }

    public function store(ParceriasRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Parceria::upload_imagem();

            Parceria::create($input);

            return redirect()->route('parcerias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Parceria $parceria)
    {
        return view('painel.parcerias.edit', compact('parceria'));
    }

    public function update(ParceriasRequest $request, Parceria $parceria)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Parceria::upload_imagem();

            $parceria->update($input);

            return redirect()->route('parcerias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Parceria $parceria)
    {
        try {
            $parceria->delete();

            return redirect()->route('parcerias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
