<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfiguracoesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_pt'                   => 'required',
            'title_en'                   => 'required',
            'title_es'                   => 'required',
            'description_pt'             => '',
            'description_en'             => '',
            'description_es'             => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => 'image',
            'analytics_ua'               => '',
            'analytics_g'                => '',
            'codigo_gtm'                 => '',
            'pixel_facebook'             => '',
            'tinify_key'                 => '',
        ];
    }
}
