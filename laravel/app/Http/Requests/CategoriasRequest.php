<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo_pt'           => 'required',
            'titulo_en'           => 'required',
            'titulo_es'           => 'required',
            'texto_pt'            => 'required',
            'texto_en'            => '',
            'texto_es'            => '',
            'vantagem1_icone'     => 'required',
            'vantagem1_titulo_pt' => 'required',
            'vantagem1_titulo_en' => '',
            'vantagem1_titulo_es' => '',
            'vantagem1_texto_pt'  => 'required',
            'vantagem1_texto_en'  => '',
            'vantagem1_texto_es'  => '',
            'vantagem2_icone'     => 'required',
            'vantagem2_titulo_pt' => 'required',
            'vantagem2_titulo_en' => '',
            'vantagem2_titulo_es' => '',
            'vantagem2_texto_pt'  => 'required',
            'vantagem2_texto_en'  => '',
            'vantagem2_texto_es'  => '',
            'vantagem3_icone'     => 'required',
            'vantagem3_titulo_pt' => 'required',
            'vantagem3_titulo_en' => '',
            'vantagem3_titulo_es' => '',
            'vantagem3_texto_pt'  => 'required',
            'vantagem3_texto_en'  => '',
            'vantagem3_texto_es'  => '',
        ];
    }
}
