<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'capa'      => 'required|image',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'texto_pt'  => 'required',
            'texto_en'  => '',
            'texto_es'  => '',
            'imagem_pt' => 'required|image',
            'imagem_en' => 'image',
            'imagem_es' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['imagem_pt'] = 'image';
            $rules['imagem_en'] = 'image';
            $rules['imagem_es'] = 'image';
        }

        return $rules;
    }
}
