<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'capa'           => 'required|image',
            'frase1_capa_pt' => 'required',
            'frase1_capa_en' => '',
            'frase1_capa_es' => '',
            'frase2_capa_pt' => 'required',
            'frase2_capa_en' => '',
            'frase2_capa_es' => '',
            'frase3_capa_pt' => 'required',
            'frase3_capa_en' => '',
            'frase3_capa_es' => '',
            'frase_edc_pt'   => 'required',
            'frase_edc_en'   => '',
            'frase_edc_es'   => '',
            'motivos1_pt'    => 'required',
            'motivos1_en'    => '',
            'motivos1_es'    => '',
            'motivos2_pt'    => 'required',
            'motivos2_en'    => '',
            'motivos2_es'    => '',
            'motivos3_pt'    => 'required',
            'motivos3_en'    => '',
            'motivos3_es'    => '',
            'frase_selo_pt'  => 'required',
            'frase_selo_en'  => '',
            'frase_selo_es'  => '',
            'imagem_vagas'   => 'required|image',
            'frase_vagas_pt' => 'required',
            'frase_vagas_en' => '',
            'frase_vagas_es' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['imagem_vagas'] = 'image';
        }

        return $rules;
    }
}
