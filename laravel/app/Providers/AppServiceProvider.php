<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use App\Models\Categoria;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\ContrateConosco;
use App\Models\ServicoMaisInfo;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
   		return realpath(base_path().'/../www');
	});
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('frontend.*', function($view) {
            $view->with('contato', Contato::first());
            $view->with('categorias', Categoria::orderBy('id', 'asc')->get());

	    $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        View::composer('painel.layout.*', function ($view) {
            $view->with('contatosContrateNaoLidos', ContrateConosco::naoLidos()->count());
            $view->with('contatosServicosNaoLidos', ServicoMaisInfo::naoLidos()->count());
        });
    }
}
