<?php

namespace App\Providers;

use App\Models\Categoria;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\ContrateConosco;
use App\Models\Home;
use App\Models\Servico;
use App\Models\ServicoMaisInfo;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';
    public const PAINEL = '/painel';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });

        Route::model('maisInfosServico', ServicoMaisInfo::class);
        Route::model('contatoContrate', ContrateConosco::class);
        Route::model('servico', Servico::class);
        Route::model('categoria', Categoria::class);
        Route::model('home', Home::class);
        Route::model('contato', Contato::class);
        Route::model('configuracao', Configuracao::class);
        Route::model('usuario', User::class);
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
