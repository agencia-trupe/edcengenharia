<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    use HasFactory;

    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public static function upload_capa()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa', [
                [
                    'width'  => 310,
                    'height' => null,
                    'path'   => 'assets/img/servicos/mini/'
                ],
                [
                    'width'  => 500,
                    'height' => null,
                    'path'   => 'assets/img/servicos/'
                ]
            ]);
        } else {
            return CropImage::make('capa', [
                [
                    'width'  => 310,
                    'height' => null,
                    'path'   => 'assets/img/servicos/mini/'
                ],
                [
                    'width'  => 500,
                    'height' => null,
                    'path'   => 'assets/img/servicos/'
                ]
            ]);
        }
    }

    public static function upload_imagem_pt()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_pt', [
                'width'  => 420,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('imagem_pt', [
                'width'  => 420,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_imagem_en()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_en', [
                'width'  => 420,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('imagem_en', [
                'width'  => 420,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }

    public static function upload_imagem_es()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_es', [
                'width'  => 420,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('imagem_es', [
                'width'  => 420,
                'height' => null,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }
}
