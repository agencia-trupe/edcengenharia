<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $table = 'categorias';

    protected $guarded = ['id'];

    public static function upload_icone1()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('vantagem1_icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/categorias/'
            ]);
        } else {
            return CropImage::make('vantagem1_icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/categorias/'
            ]);
        }
    }

    public static function upload_icone2()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('vantagem2_icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/categorias/'
            ]);
        } else {
            return CropImage::make('vantagem2_icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/categorias/'
            ]);
        }
    }

    public static function upload_icone3()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('vantagem3_icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/categorias/'
            ]);
        } else {
            return CropImage::make('vantagem3_icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/categorias/'
            ]);
        }
    }
}
