<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parceria extends Model
{
    use HasFactory;

    protected $table = 'parcerias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/parcerias/'
            ]);
        } else {
            return CropImage::make('imagem', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/parcerias/'
            ]);
        }
    }
}
